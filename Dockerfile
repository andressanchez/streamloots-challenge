FROM node:14

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm run build

COPY . .

EXPOSE 8080

CMD [ "node", "dist/index.js" ]