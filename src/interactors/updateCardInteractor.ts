import Card from "../entities/Card";
import CardRepository from "../repositories/cardRepository";
import NotifierRepository from "../repositories/notifierRepository";

const updateCardInteractor =
  (
    cardRepository: CardRepository,
    notifierRepositories: NotifierRepository[]
  ) =>
  async (
    id: string,
    name: string,
    imageUrl: string,
    rarity: string,
    quantity: Number,
    published: boolean,
    owner: string
  ): Promise<Card> => {
    const card: Card = new Card(
      owner,
      published,
      id,
      name,
      imageUrl,
      rarity,
      quantity
    );

    // persist card in mongo
    const updateCard = await cardRepository.updateCard(card);

    // notify services
    notifierRepositories.forEach((notifyRepository) =>
      notifyRepository.notifyCreation(updateCard)
    );

    // return creted card
    return updateCard;
  };

export default updateCardInteractor;
