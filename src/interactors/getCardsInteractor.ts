import Card from "../entities/Card";
import CardRepository from "../repositories/cardRepository";

const getCardsInteractor =
  (cardRepository: CardRepository) =>
  async (owner: string): Promise<Card[]> => {
    return await cardRepository.getCards(owner);
  };

export default getCardsInteractor;
