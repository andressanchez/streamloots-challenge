import CardRepository from "../repositories/cardRepository";
import NotifierRepository from "../repositories/notifierRepository";

const deleteCardInteractor =
  (
    cardRepository: CardRepository,
    notifierRepositories: NotifierRepository[]
  ) =>
  async (userId: string, cardId: string): Promise<Boolean> => {
    // persist card in mongo
    const deleted = await cardRepository.deleteCard(userId, cardId);

    // notify services
    notifierRepositories.forEach((notifyRepository) =>
      notifyRepository.notifyDeletion(cardId)
    );

    // return creted card
    return deleted;
  };

export default deleteCardInteractor;
