import createCardInteractor from "./createCardInteractor";
import MongoCardDataSource from "../dataSources/mongoCardDataSource";
import CardRepository from "../repositories/cardRepository";
import getCardsInteractor from "./getCardsInteractor";
import deleteCardInteractor from "./deleteCardInteractor";
import getCardInteractor from "./getCardInteractor";
import MixPanelNotifier from "../notifiers/MixPanelNotifier";
import HeapNotifier from "../notifiers/HeapNotifier";
import updateCardInteractor from "./updateCardInteractor";
import getCardsUserInteractor from "./getCardsUserInteractor";

let cardRepository: CardRepository = new MongoCardDataSource();
let notifierRepositories = [new MixPanelNotifier(), new HeapNotifier()];

let createCard = createCardInteractor(cardRepository, notifierRepositories);
let updateCard = updateCardInteractor(cardRepository, notifierRepositories);
let getCard = getCardInteractor(cardRepository);
let getCards = getCardsInteractor(cardRepository);
let deleteCard = deleteCardInteractor(cardRepository, notifierRepositories);

let getCardsUser = getCardsUserInteractor(cardRepository);

export { createCard, updateCard, getCard, getCards, deleteCard, getCardsUser };
