import Card from "../entities/Card";
import CardRepository from "../repositories/cardRepository";
import NotifierRepository from "../repositories/notifierRepository";

const createCardInteractor =
  (
    cardRepository: CardRepository,
    notifierRepositories: NotifierRepository[]
  ) =>
  async (
    name: string,
    imageUrl: string,
    rarity: string,
    quantity: Number,
    published: boolean,
    owner: string
  ): Promise<Card> => {
    const card: Card = new Card(
      owner,
      published,
      undefined,
      name,
      imageUrl,
      rarity,
      quantity
    );

    // persist card in mongo
    const createdCard = await cardRepository.createCard(card);

    // notify services
    notifierRepositories.forEach((notifyRepository) =>
      notifyRepository.notifyCreation(createdCard)
    );

    // return creted card
    return createdCard;
  };

export default createCardInteractor;
