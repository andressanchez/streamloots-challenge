import Card from "../entities/Card";
import CardRepository from "../repositories/cardRepository";

const getCardInteractor =
  (cardRepository: CardRepository) =>
  async (owner: string, cardId: string): Promise<Card> => {
    return await cardRepository.getCard(owner, cardId);
  };

export default getCardInteractor;
