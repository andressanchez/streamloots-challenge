import Card from "../entities/Card";
import CardRepository from "../repositories/cardRepository";

const getCardsUserInteractor =
  (cardRepository: CardRepository) =>
  async (user: string): Promise<Card[]> => {
    return await cardRepository.getCards(user);
  };

export default getCardsUserInteractor;
