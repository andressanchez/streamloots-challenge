import express from "express";
import helmet from "helmet";
import compression from "compression";
import cors from "cors";
import CreateCardController from "./controllers/createCardController";
import UpdateCardController from "./controllers/updateCardController";
import GetCardController from "./controllers/getCardController";
import GetCardsController from "./controllers/getCardsController";
import DeleteCardController from "./controllers/deleteCardController";
import GetCardsUserController from "./controllers/getCardsUserController";

class Server {
  app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.routes();
  }

  config() {
    this.app.set("port", process.env.API_PORT || 8080);
    this.app.use(express.json());
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(cors());
  }

  routes() {
    // Endpoints for card manipulation by owner
    this.app.post("/cards", CreateCardController);
    this.app.put("/cards/:cardId", UpdateCardController);
    this.app.get("/cards", GetCardsController);
    this.app.get("/cards/:cardId", GetCardController);
    this.app.delete("/cards/:cardId", DeleteCardController);

    // Endpoint to retrieve published cards under a user
    this.app.get("/users/:userId/cards", GetCardsUserController);
  }

  start() {
    this.app.listen(this.app.get("port"), () => {
      console.log("Server on port ", this.app.get("port"));
    });
  }
}

const server = new Server();
server.start();
