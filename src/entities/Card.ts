export default class Card {
  private owner: string;
  private published: boolean;
  private id?: string;
  private name?: string;
  private image?: string;
  private rarity?: string;
  private quantity?: Number;

  constructor(
    owner: string,
    published: boolean,
    id?: string,
    name?: string,
    image?: string,
    rarity?: string,
    quantity?: Number
  ) {
    // Validate object
    if (owner === undefined || owner === null) {
      throw Error("Owner must always be provided.");
    }
    if (published === undefined || published === null) {
      throw Error("Published must always be provided.");
    }
    if (rarity === "limited" && (quantity === undefined || quantity === null)) {
      throw Error("Quantity must be provided if rarity is limited.");
    }
    if (published === true) {
      if (name === undefined || name === null) {
        throw Error("Name must always be provided.");
      }
      if (image === undefined || image === null) {
        throw Error("Image must always be provided.");
      }
      if (rarity === undefined || rarity === null) {
        throw Error("Rarity must always be provided.");
      }
      if (rarity !== "regular" && rarity !== "limited") {
        throw Error("Rarity must be either regular or limited.");
      }
    }

    // Assign attributes
    this.owner = owner;
    this.published = published;
    this.id = id;
    this.name = name;
    this.image = image;
    this.rarity = rarity;
    this.quantity = quantity;
  }
}
