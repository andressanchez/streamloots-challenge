import Card from "../entities/Card";

export default interface CardRepository {
    createCard(card: Card): Promise<Card>;
    updateCard(card: Card): Promise<Card>;
    deleteCard(userId: string, cardId: string): Promise<boolean>;
    getCard(userId: string, cardId: string): Promise<Card>;
    getCards(userId: string): Promise<Card[]>;
}