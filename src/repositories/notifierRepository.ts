import Card from "../entities/Card";

export default interface NotifierRepository {
  notifyCreation(card: Card): void;
  notifyEdition(card: Card): void;
  notifyDeletion(cardId: string): void;
}
