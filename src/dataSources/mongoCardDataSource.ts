import Card from "../entities/Card";
import CardRepository from "../repositories/cardRepository";
import MongoClient, { ObjectId } from "mongodb";

export default class MongoCardDataSource implements CardRepository {
  collection!: MongoClient.Collection<Card>;

  async createCard(card: Card): Promise<Card> {
    const collection = await this.getCollection();
    await collection.save(card);
    return card;
  }

  async updateCard(card: Card): Promise<Card> {
    const collection = await this.getCollection();
    await collection.save(card);
    return card;
  }

  async deleteCard(userId: string, cardId: string): Promise<boolean> {
    const collection = await this.getCollection();
    await collection.deleteOne({ _id: new ObjectId(cardId), owner: userId });
    return true;
  }

  async getCards(userId: string): Promise<Card[]> {
    const collection = await this.getCollection();
    const cursor = collection.find({ owner: userId });
    return await cursor.toArray();
  }

  async getCard(userId: string, cardId: string): Promise<Card> {
    console.log("id: " + cardId);
    const collection = await this.getCollection();
    const card = await collection.findOne({ _id: new ObjectId(cardId), owner: userId });
    if (card === null) {
      throw Error("Card not found");
    }
    return card;
  }

  private async getCollection() {
    if (this.collection === undefined) {
      const url = "mongodb://mongodb:27017";
      const client = await MongoClient.connect(url, {
        useUnifiedTopology: true,
      });
      const db = client.db("streamloots");
      this.collection = db.collection("cards");
    }
    return this.collection;
  }
}
