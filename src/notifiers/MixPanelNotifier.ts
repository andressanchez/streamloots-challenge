import Card from "../entities/Card";
import NotifierRepository from "../repositories/notifierRepository";

export default class MixPanelNotifier implements NotifierRepository {
  notifyCreation(card: Card): void {
    console.log("MixPanel: Card Created: " + card);
  }
  notifyEdition(card: Card): void {
    console.log("MixPanel: Card Edited: " + card);
  }
  notifyDeletion(card: string): void {
    console.log("MixPanel: Card Deleted: " + card);
  }
}
