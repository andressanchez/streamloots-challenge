import Card from "../entities/Card";
import NotifierRepository from "../repositories/notifierRepository";

export default class HeapNotifier implements NotifierRepository {
  notifyCreation(card: Card): void {
    console.log("Heap: Card Created: " + card);
  }
  notifyEdition(card: Card): void {
    console.log("Heap: Card Edited: " + card);
  }
  notifyDeletion(card: string): void {
    console.log("Heap: Card Deleted: " + card);
  }
}
