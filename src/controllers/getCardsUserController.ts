import { getCardsUser } from "../interactors";
import { Response, Request } from "express";
import Card from "../entities/Card";
import jwt from "jsonwebtoken";

const GetCardsUserController = async (request: Request, response: Response) => {
  const authorizationHeader: string = <string>request.header("authorization");

  // An authorization header is required
  if (
    authorizationHeader === undefined ||
    !authorizationHeader.startsWith("Bearer ")
  ) {
    response.statusCode = 401;
    response.json({
      error: "Token required",
    });
    return;
  }

  // Extract userId from JWT
  const payload = jwt.decode(authorizationHeader.substring(7)) as {
    userId: string;
  };

  if (payload === null || payload.userId === null) {
    response.statusCode = 401;
    response.json({
      error: "userId required",
    });
    return;
  }

  // Get cardId from path
  const targetId = request.params.userId;

  // Get cards
  try {
    const cards: Card[] = await getCardsUser(targetId);
    response.json(cards);
  } catch (e) {
    response.statusCode = 400;
    response.json({
      error: e.message,
    });
  }
};

export default GetCardsUserController;
