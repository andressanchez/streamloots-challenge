import { updateCard } from "../interactors";
import { Response, Request } from "express";
import Card from "../entities/Card";
import jwt from "jsonwebtoken";

const UpdateCardController = async (request: Request, response: Response) => {
  const { body } = request;
  const { name, image, rarity, quantity, published } = body;

  const authorizationHeader: string = <string>request.header("authorization");

  // An authorization header is required
  if (
    authorizationHeader === undefined ||
    !authorizationHeader.startsWith("Bearer ")
  ) {
    response.statusCode = 401;
    response.json({
      error: "Token required",
    });
    return;
  }

  // Extract userId from JWT
  const payload = jwt.decode(authorizationHeader.substring(7)) as {
    userId: string;
  };

  if (payload === null || payload.userId === null) {
    response.statusCode = 401;
    response.json({
      error: "userId required",
    });
    return;
  }

  // Get cardId from path
  const cardId = request.params.cardId;

  // Update card
  try {
    const card: Card = await updateCard(
      cardId,
      name,
      image,
      rarity,
      quantity,
      published,
      payload.userId
    );
    response.json(card);
  } catch (e) {
    response.statusCode = 400;
    response.json({
      error: e.message,
    });
  }
};

export default UpdateCardController;
